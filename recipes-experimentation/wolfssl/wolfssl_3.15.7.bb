
SUMMARY = "wolfSSL Lightweight Embedded SSL/TLS Library"

DESCRIPTION = "wolfSSL, formerly CyaSSL, is a lightweight SSL library written \
               in C and optimized for embedded and RTOS environments. It can \
               be up to 20 times smaller than OpenSSL while still supporting \
               a full TLS client and server, up to TLS 1.3"

HOMEPAGE = "https://www.wolfssl.com/products/wolfssl"
BUGTRACKER = "https://github.com/wolfssl/wolfssl/issues"

SECTION = "libs"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

PATH_prepend = "${STAGING_BINDIR_TOOLCHAIN}.${STAGINGCC}:"

DEPENDS_append_toolchain-clang = "clang-cross-${TARGET_ARCH}"
TOOLCHAIN ?= "clang"
TCLIBC = "musl"
ARM_INSTRUCTION_SET_toolchain-clang = "arm"

PROVIDES += "cyassl"

RPROVIDES_${PN} = "cyassl"

SRCREV = "${AUTOREV}"
BRANCH = "master"
SRC_URI = "git://${HOME}/HardBlare/applications-hardblare/wolfssl-3.15.7/;protocol=file;barcelone=1;branch=${BRANCH}"
S = "${WORKDIR}/git"

DISABLE_STATIC=""

CFLAGS = " -mno-thumb -mllvm -hardblare-annot -mllvm -hardblare-instr -v "
CXXFLAGS = " -mno-thumb -mllvm -hardblare-annot -mllvm -hardblare-instr -v "

# CFLAGS += " -fPIC -L${STAGING_DIR_TARGET}${libdir}/hardblare/libc.a -mfpu=neon -mfloat-abi=hard -mcpu=cortex-a9 -mlittle-endian --sysroot=${STAGING_DIR_TARGET} -mno-thumb -mllvm -hardblare-annot -mllvm -hardblare-instr -v "
# CXXFLAGS += " -fPIC -L${STAGING_DIR_TARGET}${libdir}/hardblare/libc.a -mfpu=neon -mfloat-abi=hard -mcpu=cortex-a9 -mlittle-endian --sysroot=${STAGING_DIR_TARGET} -mno-thumb -mllvm -hardblare-annot -mllvm -hardblare-instr -v "

EXTRA_OECONF += " \
	     --disable-shared \
	     --enable-static \
	     --enable-nginx \
	     --enable-singlethreaded \
	     --enable-opensslextra \
	     --prefix=${prefix} \
	     --exec-prefix=${exec_prefix} \
	     --bindir=${bindir} \
	     --libdir=${libdir} \
	     --includedir=${includedir} \
"


inherit autotools

BBCLASSEXTEND += "native nativesdk"

