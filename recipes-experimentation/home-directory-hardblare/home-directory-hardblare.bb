############################################################
#
#  Recipe for the home directory: compile and install
#  all needed files for testing HardBlare.
#
#  Copyright - 2019
#  Mounir NASR ALLAH <mounir.nasrallah@centralesupelec.fr>
#  cp -r ${S} ${D}/home/root/
#
############################################################

DESCRIPTION = "HardBlare home directory"
LICENSE = "GPLv2"

LIC_FILES_CHKSUM = "file://COPYING;md5=5db9fa4242939313e76e8de679431dac"


TOOLCHAIN ?= "clang"
TCLIBC = "musl"
ARM_INSTRUCTION_SET_toolchain-clang = "arm"

CFLAGS = " -L${STAGING_DIR_TARGET}${libdir}/hardblare/libc.a -static -mno-thumb -mllvm -hardblare-instr -mllvm -hardblare-annot "
CXXFLAGS = " -L${STAGING_DIR_TARGET}${libdir}/hardblare/libc.a -static -mno-thumb -mllvm -hardblare-instr -mllvm -hardblare-annot "

SRCREV = "${AUTOREV}"
BRANCH = "master"

SRC_URI = "git://${HOME}/HardBlare/home-directory-hardblare;protocol=file;barcelone=1;branch=${BRANCH}"
S = "${WORKDIR}/git"


do_compile () {
	   oe_runmake all
}


FILES_${PN} ="*"

do_install () {
	   oe_runmake install 'DESTDIR=${D}'
}
