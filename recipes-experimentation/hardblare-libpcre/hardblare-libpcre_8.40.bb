DESCRIPTION = "The PCRE library is a set of functions that implement regular \
expression pattern matching using the same syntax and semantics as Perl 5. PCRE \
has its own native API, as well as a set of wrapper functions that correspond \
to the POSIX regular expression API."
SUMMARY = "Perl Compatible Regular Expressions"
HOMEPAGE = "http://www.pcre.org"
SECTION = "devel"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENCE;md5=60da32d84d067f53e22071c4ecb4384d"
SRC_URI = "ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-${PV}.tar.bz2 \
           file://pcre-cross.patch \
           file://fix-pcre-name-collision.patch \
           file://run-ptest \
           file://Makefile \
"

PATH_prepend = "${STAGING_BINDIR_TOOLCHAIN}.${STAGINGCC}:"
DEPENDS_append_class-target = "clang-cross-${TARGET_ARCH}"
TOOLCHAIN ?= "clang"
TCLIBC = "musl"
ARM_INSTRUCTION_SET_toolchain-clang = "arm"

CFLAGS_append_class-target += " -L${STAGING_DIR_TARGET}${libdir}/hardblare/libc.a -mno-thumb -mllvm -hardblare-instr -mllvm -hardblare-annot -fPIC "
CXXFLAGS_append_class-target += " -L${STAGING_DIR_TARGET}${libdir}/hardblare/libc.a -mno-thumb -mllvm -hardblare-instr -mllvm -hardblare-annot -fPIC "


SRC_URI[md5sum] = "41a842bf7dcecd6634219336e2167d1d"
SRC_URI[sha256sum] = "00e27a29ead4267e3de8111fcaa59b132d0533cdfdbdddf4b0604279acbcf4f4"

CVE_PRODUCT = "hardblare-pcre"

S = "${WORKDIR}/hardblare-pcre-${PV}"

PROVIDES += "hardblare-pcre"
DEPENDS += " bzip2 zlib "

PACKAGECONFIG ??= "pcre8 unicode-properties"

PACKAGECONFIG[pcre8] = "--enable-pcre8,--disable-pcre8"
PACKAGECONFIG[pcre16] = "--enable-pcre16,--disable-pcre16"
PACKAGECONFIG[pcre32] = "--enable-pcre32,--disable-pcre32"
PACKAGECONFIG[pcretest-readline] = "--enable-pcretest-libreadline,--disable-pcretest-libreadline,readline,"
PACKAGECONFIG[unicode-properties] = "--enable-unicode-properties,--disable-unicode-properties"

BINCONFIG = "${bindir}/pcre-config"

inherit autotools binconfig-disabled ptest

EXTRA_OECONF = "\
    --enable-newline-is-lf \
    --enable-rebuild-chartables \
    --enable-utf \
    --with-link-size=2 \
    --with-match-limit=10000000 \
    --disable-cpp \
    --disable-jit \
    --disable-shared \
    --enable-static \
    --disable-pcregrep-jit \
    --with-pic \
 "

# Set LINK_SIZE in BUILD_CFLAGS given that the autotools bbclass use it to
# set CFLAGS_FOR_BUILD, required for the libpcre build.
BUILD_CFLAGS =+ "-DLINK_SIZE=2 -I${B}"
CFLAGS += "-D_REENTRANT"
CXXFLAGS_append_powerpc = " -lstdc++"

export CCLD_FOR_BUILD ="${BUILD_CCLD}"

PACKAGES =+ "libpcrecpp libpcreposix pcregrep pcregrep-doc pcretest pcretest-doc"

SUMMARY_libpcrecpp = "${SUMMARY} - C++ wrapper functions"
SUMMARY_libpcreposix = "${SUMMARY} - C wrapper functions based on the POSIX regex API"
SUMMARY_pcregrep = "grep utility that uses perl 5 compatible regexes"
SUMMARY_pcregrep-doc = "grep utility that uses perl 5 compatible regexes - docs"
SUMMARY_pcretest = "program for testing Perl-comatible regular expressions"
SUMMARY_pcretest-doc = "program for testing Perl-comatible regular expressions - docs"

FILES_libpcrecpp = "${libdir}/hardblare/libpcrecpp.so.*"
FILES_libpcreposix = "${libdir}/hardblare/libpcreposix.so.*"
FILES_pcregrep = "${bindir}/hardblare/pcregrep"
FILES_pcregrep-doc = "${mandir}/man1/hardblare/pcregrep.1"
FILES_pcretest = "${bindir}/hardblare/pcretest"
FILES_pcretest-doc = "${mandir}/man1/hardblare/pcretest.1"

BBCLASSEXTEND = "native nativesdk"

do_install_ptest() {
	t=${D}${PTEST_PATH}/hardblare
	cp ${WORKDIR}/Makefile $t
	cp -r ${S}/testdata $t
	for i in RunTest RunGrepTest test-driver; \
	  do cp ${S}/$i $t; \
	done
}
