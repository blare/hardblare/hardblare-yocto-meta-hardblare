/*
 * argv.h
 *
 * Copyright (c) 2001 Dug Song <dugsong@monkey.org>
 *
 * $Id: argv.h 2000 2008-04-27 06:17:35Z aturner $
 */

#ifndef ARGV_H
#define ARGV_H

int	 argv_create(char *p, int argc, char *argv[]);
char	*argv_copy(char *argv[]);

#endif /* ARGV_H */
