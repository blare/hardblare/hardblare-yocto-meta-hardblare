SUMMARY = "HTTP and reverse proxy server"

DESCRIPTION = "Nginx is a web server and a reverse proxy server for \
HTTP, SMTP, POP3 and IMAP protocols, with a strong focus on high  \
concurrency, performance and low memory usage."

HOMEPAGE = "http://nginx.org/"
LICENSE = "BSD-2-Clause"

SECTION = "net"

DEPENDS = "openssl"

SRC_URI = " \
    http://nginx.org/download/nginx-${PV}.tar.gz \
    file://nginx.conf \
    file://nginx.init \
    file://nginx-volatile.conf \
    file://nginx.service \
"

inherit update-rc.d useradd systemd

SYSTEMD_SERVICE_${PN} = "nginx.service"

INHIBIT_PACKAGE_DEBUG_SPLIT = '1'
INHIBIT_PACKAGE_STRIP = '1'

PATH_prepend = "${STAGING_BINDIR_TOOLCHAIN}.${STAGINGCC}:"

TOOLCHAIN ?= "clang"
TCLIBC = "musl"

ARM_INSTRUCTION_SET_toolchain-clang = "arm"

CFLAGS_append = " -fPIE -pie"
CXXFLAGS_append = " -fPIE -pie"

# CFLAGS_append = " -L${STAGING_DIR_TARGET}${libdir}/libc-hardblare.a --sysroot=${STAGING_DIR_TARGET} -mno-thumb -mllvm -hardblare-instr -mllvm -hardblare-annot -fPIE -pie "
# CXXFLAGS_append = " -L${STAGING_DIR_TARGET}${libdir}/libc-hardblare.a --sysroot=${STAGING_DIR_TARGET} -mno-thumb -mllvm -hardblare-instr -mllvm -hardblare-annot -fPIE -pie "
LDFLAGS_append = ""


CFLAGS += " -L${STAGING_DIR_TARGET}${libdir}/ -L${STAGING_DIR_TARGET}${libdir}/hardblare/libc.a -mfpu=neon -mfloat-abi=hard -mcpu=cortex-a9 -mlittle-endian --sysroot=${STAGING_DIR_TARGET} -fPIC -mno-thumb -mllvm -hardblare-annot -mllvm -hardblare-instr -v "

CXXFLAGS += "  -L${STAGING_DIR_TARGET}${libdir}/ -L${STAGING_DIR_TARGET}${libdir}/hardblare/libc.a -mfpu=neon -mfloat-abi=hard -mcpu=cortex-a9 -mlittle-endian --sysroot=${STAGING_DIR_TARGET} -fPIC -mno-thumb -mllvm -hardblare-annot -mllvm -hardblare-instr -v "

NGINX_WWWDIR ?= "${localstatedir}/www/localhost"
NGINX_USER   ?= "www"

EXTRA_OECONF = ""
DISABLE_STATIC = ""

# PACKAGECONFIG[http2] = "--with-http_v2_module,,"

do_configure () {
    if [ "${SITEINFO_BITS}" = "64" ]; then
        PTRSIZE=8
    else
        PTRSIZE=4
    fi

    echo $CFLAGS
    echo $LDFLAGS

    # Add the LDFLAGS to the main nginx link to avoid issues with missing GNU_HASH
    #    echo "MAIN_LINK=\"\${MAIN_LINK} ${LDFLAGS}\"" >> auto/cc/conf

    ./configure \
    --with-openssl-opt="no-krb5 no-ssl2 no-shared" \ 
    --with-cc-opt="-O2 -static -static-libgcc" \
    --with-ld-opt=" -L${STAGING_DIR_TARGET}${libdir}/hardblare/libc.a -static -Bstatic -static -static-libgcc -static-libstdc++ -static -Bstatic -fPIE -pie " \
    --without-http_rewrite_module    \
    --without-select_module \
    --without-poll_module \
    --without-http_proxy_module \
    --without-http_fastcgi_module \
    --without-http_gzip_module \
    --without-select_module \ 
    --without-http_rewrite_module \
    --without-http_charset_module \
    --without-http_gzip_module \
    --without-http_ssi_module \
    --without-http_userid_module \
    --without-http_access_module \
    --without-http_autoindex_module \
    --without-http_geo_module \
    --without-http_map_module \
    --without-http_split_clients_module \
    --without-http_referer_module \
    --without-http_proxy_module \
    --without-http_fastcgi_module \
    --without-http_uwsgi_module \
    --without-http_scgi_module \
    --without-http_memcached_module \
    --without-http_limit_conn_module \
    --without-http_limit_req_module \
    --without-http_empty_gif_module \
    --without-http_browser_module \
    --without-http_upstream_hash_module \
    --without-http_upstream_ip_hash_module \
    --without-http_upstream_least_conn_module \
    --without-http_upstream_keepalive_module \
    --without-http_upstream_zone_module \
    --without-http-cache \
    --without-mail_pop3_module \
    --without-mail_imap_module \
    --without-mail_smtp_module \
    --without-stream_limit_conn_module \
    --without-stream_access_module \
    --without-stream_upstream_hash_module \
    --without-stream_upstream_least_conn_module \
    --without-stream_upstream_zone_module \
    --without-http-cache \
   	 \
    --crossbuild=Linux:${TUNE_ARCH} \
    --with-endian=${@base_conditional('SITEINFO_ENDIANNESS', 'le', 'little', 'big', d)} \
    --with-int=4 \
    --with-long=${PTRSIZE} \
    --with-long-long=8 \
    --with-ptr-size=${PTRSIZE} \
    --with-sig-atomic-t=${PTRSIZE} \
    --with-size-t=${PTRSIZE} \
    --with-off-t=${PTRSIZE} \
    --with-time-t=${PTRSIZE} \
    --with-sys-nerr=132 \
    --conf-path=${sysconfdir}/nginx/nginx.conf \
    --http-log-path=${localstatedir}/log/nginx/access.log \
    --error-log-path=${localstatedir}/log/nginx/error.log \
    --http-client-body-temp-path=/run/nginx/client_body_temp \
    --http-proxy-temp-path=/run/nginx/proxy_temp \
    --http-fastcgi-temp-path=/run/nginx/fastcgi_temp \
    --http-uwsgi-temp-path=/run/nginx/uwsgi_temp \
    --http-scgi-temp-path=/run/nginx/scgi_temp \
    --pid-path=/run/nginx/nginx.pid \
    --prefix=${prefix} \
    --with-cpu-opt=generic \
    ${EXTRA_OECONF}
}

do_compile () {
	   make build
}

do_install () {
    oe_runmake 'DESTDIR=${D}' install
    rm -fr ${D}${localstatedir}/run ${D}/run
    if ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'true', 'false', d)}; then
        install -d ${D}${sysconfdir}/tmpfiles.d
        echo "d /run/${BPN} - - - -" \
            > ${D}${sysconfdir}/tmpfiles.d/${BPN}.conf
        echo "d /${localstatedir}/log/${BPN} 0755 root root -" \
            >> ${D}${sysconfdir}/tmpfiles.d/${BPN}.conf
    fi
    install -d ${D}${sysconfdir}/${BPN}
    ln -snf ${localstatedir}/run/${BPN} ${D}${sysconfdir}/${BPN}/run
    install -d ${D}${NGINX_WWWDIR}
    mv ${D}/usr/html ${D}${NGINX_WWWDIR}/
    chown ${NGINX_USER}:www-data -R ${D}${NGINX_WWWDIR}

    install -d ${D}${sysconfdir}/init.d
    install -m 0755 ${WORKDIR}/nginx.init ${D}${sysconfdir}/init.d/nginx
    sed -i 's,/usr/sbin/,${sbindir}/,g' ${D}${sysconfdir}/init.d/nginx
    sed -i 's,/etc/,${sysconfdir}/,g'  ${D}${sysconfdir}/init.d/nginx

    install -d ${D}${sysconfdir}/nginx
    install -m 0644 ${WORKDIR}/nginx.conf ${D}${sysconfdir}/nginx/nginx.conf
    sed -i 's,/var/,${localstatedir}/,g' ${D}${sysconfdir}/nginx/nginx.conf
    sed -i 's/^user.*/user ${NGINX_USER};/g' ${D}${sysconfdir}/nginx/nginx.conf
    install -d ${D}${sysconfdir}/nginx/sites-enabled

    install -d ${D}${sysconfdir}/default/volatiles
    install -m 0644 ${WORKDIR}/nginx-volatile.conf ${D}${sysconfdir}/default/volatiles/99_nginx
    sed -i 's,/var/,${localstatedir}/,g' ${D}${sysconfdir}/default/volatiles/99_nginx
    sed -i 's,@NGINX_USER@,${NGINX_USER},g' ${D}${sysconfdir}/default/volatiles/99_nginx

    if ${@bb.utils.contains('DISTRO_FEATURES','systemd','true','false',d)};then
        install -d ${D}${systemd_unitdir}/system
        install -m 0644 ${WORKDIR}/nginx.service ${D}${systemd_unitdir}/system/
        sed -i -e 's,@SYSCONFDIR@,${sysconfdir},g' \
            -e 's,@LOCALSTATEDIR@,${localstatedir},g' \
            -e 's,@SBINDIR@,${sbindir},g' \
            ${D}${systemd_unitdir}/system/nginx.service
    fi
}

pkg_postinst_${PN} () {
    if [ -z "$D" ]; then
        if type systemd-tmpfiles >/dev/null; then
            systemd-tmpfiles --create
        elif [ -e ${sysconfdir}/init.d/populate-volatile.sh ]; then
            ${sysconfdir}/init.d/populate-volatile.sh update
        fi
    fi
}

FILES_${PN} += " \
    ${localstatedir}/ \
    ${systemd_unitdir}/system/nginx.service \
"

CONFFILES_${PN} = " \
    ${sysconfdir}/nginx/nginx.conf \
    ${sysconfdir}/nginx/fastcgi.conf \
    ${sysconfdir}/nginx/fastcgi_params \
    ${sysconfdir}/nginx/koi-utf \
    ${sysconfdir}/nginx/koi-win \
    ${sysconfdir}/nginx/mime.types \
    ${sysconfdir}/nginx/scgi_params \
    ${sysconfdir}/nginx/uwsgi_params \
    ${sysconfdir}/nginx/win-utf \
"

INITSCRIPT_NAME = "nginx"
INITSCRIPT_PARAMS = "defaults 92 20"

USERADD_PACKAGES = "${PN}"
USERADD_PARAM_${PN} = " \
    --system --no-create-home \
    --home ${NGINX_WWWDIR} \
    --groups www-data \
    --user-group ${NGINX_USER}"
