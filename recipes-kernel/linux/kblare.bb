LINUX_VERSION = "4.9"
XILINX_RELEASE_VERSION = "v2017.1"
SRCREV="${AUTOREV}"


# This version extension should match CONFIG_LOCALVERSION in defconfig
XILINX_RELEASE_VERSION ?= ""
LINUX_VERSION_EXTENSION ?= "-xilinx-${XILINX_RELEASE_VERSION}"
PV = "${LINUX_VERSION}${LINUX_VERSION_EXTENSION}+git${SRCPV}"

# Sources, by default allow for the use of SRCREV pointing to orphaned tags/commits

FILESOVERRIDES_append = ":${LINUX_VERSION}"

KBRANCH = "dev"

KERNELURI ?= "git://${HOME}/HardBlare/kblare-linux-4.9-hardblare;protocol=file;barcelone=1;branch=${KBRANCH}"
SRC_URI = " \
	${KERNELURI} \
	file://xilinx-base;type=kmeta;name=kmeta-xilinx-base;destsuffix=xilinx-base \
	"

SRC_URI += " \
	file://0001-drm-xilinx-Add-encoder-for-Digilent-boards.patch \
	file://0002-clk-Add-driver-for-axi_dynclk-IP-Core.patch \
	file://0003-drm-xilinx-Fix-DPMS-transition-to-on.patch \
  file://defconfig \
  "


SRCREV_machine ?= "${SRCREV}"

require recipes-kernel/linux/linux-yocto.inc

DESCRIPTION = "Xilinx Kernel"

require linux-xilinx-configs.inc
require linux-xilinx-machines.inc

KBUILD_DEFCONFIG_zcu102-zynqmp = "xilinx_zynqmp_defconfig"
KCONFIG_MODE_zcu102-zynqmp = "alldefconfig"

KERNEL_FEATURES_append = " bsp/xilinx/soc/drivers/xilinx.scc"
KERNEL_FEATURES_append_zynq = " bsp/xilinx/soc/drivers/zynq7.scc"
KERNEL_FEATURES_append_zynqmp = " \
		bsp/xilinx/soc/drivers/zynqmp.scc \
		features/drm/drm-xilinx.scc \
		features/v4l2/v4l2-xilinx.scc \
		"


SRC_URI_append_zybo-linux-bd-zynq7 = " \
        file://0001-drm-xilinx-Add-encoder-for-Digilent-boards.patch \
        file://0002-clk-Add-driver-for-axi_dynclk-IP-Core.patch \
        file://0003-drm-xilinx-Fix-DPMS-transition-to-on.patch \
        "
