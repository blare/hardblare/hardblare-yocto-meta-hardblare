SUMMARY = "HardBlare tools configuration files"
DESCRIPTION = "HardBlare tools configuration files"
HOMEPAGE = ""
SECTION = "base"
LICENSE = "CLOSED"

SRC_URI = "file://hardblare_policy"

S = "${WORKDIR}"

do_install() {
    install -d ${D}${sysconfdir}
    install -m 0644 ${WORKDIR}/hardblare_policy ${D}${sysconfdir}/hardblare_policy
}

INHIBIT_PACKAGE_STRIP = "1"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
INSANE_SKIP_${PN} += "already-stripped"
INHIBIT_SYSROOT_STRIP = "1"
