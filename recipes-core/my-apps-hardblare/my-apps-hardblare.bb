SUMMARY = "My application for HardBlare"
DESCRIPTION = "My application for HardBlare"
HOMEPAGE = ""

LICENSE = "CLOSED"

MY_FILES = "~/HardBlare/my-apps-hardblare/DAC/dac \
            ~/HardBlare/my-apps-hardblare/DAC/dac-annot.o \
            ~/HardBlare/my-apps-hardblare/DAC/dac-hardblare-segment \
            ~/HardBlare/my-apps-hardblare/fir-static-musl/fir-static-musl \
            ~/HardBlare/my-apps-hardblare/fir-static-musl/fir-static-musl-instr \
            ~/HardBlare/my-apps-hardblare/fir-static-musl/fir-dyn-musl-instr \
            ~/HardBlare/my-apps-hardblare/MiBench/*.elf \
            ~/HardBlare/my-apps-hardblare/benchmark/*.elf \
            ~/HardBlare/my-apps-hardblare/benchmark/all-memory/*.elf \
            ~/HardBlare/my-apps-hardblare/benchmark/nopc-nosp/*.elf \
            ~/HardBlare/my-apps-hardblare/benchmark/korean-like/*.elf \
"


do_install() {
install -d ${D}${datadir}/my-apps-hardblare
cp ${MY_FILES} ${D}${datadir}/my-apps-hardblare/
}

INHIBIT_PACKAGE_STRIP = "1"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
INSANE_SKIP_${PN} += "already-stripped"
INHIBIT_SYSROOT_STRIP = "1"

FILES_${PN} += "${datadir}/my-apps-hardblare"