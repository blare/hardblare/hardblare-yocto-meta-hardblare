# Copyright (C) 2014 Khem Raj <raj.khem@gmail.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require musl.inc

# mirror is at git://github.com/kraj/musl.git


DEPENDS_append_toolchain-clang = "clang-cross-${TARGET_ARCH}"

TOOLCHAIN ?= "clang"
TCLIBC = "musl"

SRCREV = "${AUTOREV}"

# OK "ffb72a4033a7ef589406c11353d24e86f1bc8b5d"
# NOK "bc63f867ba33010b96cdec4e11b14155b9a7b55f"
# OK "bba1e2d3add74de20c04d0a907a3233d947ad4f7"
# OK "dbb1d9266619b7a98322b0e12e95548717b2ae4b"
BRANCH = "dev"

PV = "1.1.16+git${SRCPV}"

# mirror is at git://github.com/kraj/musl.git


SRC_URI = "git://${HOME}/HardBlare/musl-libc-hardblare;protocol=file;barcelone=1;branch=${BRANCH} \
           file://0001-Make-dynamic-linker-a-relative-symlink-to-libc.patch \
          "

S = "${WORKDIR}/git"

PROVIDES += "virtual/libc virtual/${TARGET_PREFIX}libc-for-gcc virtual/libiconv virtual/libintl"

DEPENDS = "virtual/${TARGET_PREFIX}binutils \
           virtual/${TARGET_PREFIX}gcc-initial \
           libgcc-initial \
           linux-libc-headers \
           bsd-headers \
          "

export CROSS_COMPILE="${TARGET_PREFIX}"

LDFLAGS += "-Wl,-soname,libc.so"

ARM_INSTRUCTION_SET_toolchain-clang = "arm"

CONFIGUREOPTS = " \
    --prefix=${prefix} \
    --exec-prefix=${exec_prefix} \
    --bindir=${bindir} \
    --libdir=${libdir} \
    --includedir=${includedir} \
    --syslibdir=${base_libdir} \
    --disable-visibility \
"

do_configure() {
	       ${S}/configure ${CONFIGUREOPTS}
}

do_compile() {
	     oe_runmake
}


do_install() {
	     oe_runmake install DESTDIR='${D}'
	     install -d ${D}${bindir}

	     lnr ${D}${libdir}/libc.so ${D}${bindir}/ldd

	     for l in crypt dl m pthread resolv rt util xnet
	     do
		ln -s libc.so ${D}${libdir}/lib$l.so
	     done

}

RDEPENDS_${PN}-dev += "linux-libc-headers-dev bsd-headers-dev"
RPROVIDES_${PN}-dev += "libc-dev virtual-libc-dev"
RPROVIDES_${PN} += "ldd libsegfault rtld(GNU_HASH)"


RDEPENDS_${PN}-dev += "${PN}-staticdev"
BBCLASSEXTEND = "native nativesdk"

LEAD_SONAME = "libc.so"
