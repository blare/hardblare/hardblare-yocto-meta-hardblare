# simple blare tools recipe

DESCRIPTION = "Blare tools"
DEPENDS = "attr"
LICENSE = "GPLv2"

PACKAGES = "${PN}-dbg ${PN}"
FILES_${PN} ="*"

LIC_FILES_CHKSUM = "file://COPYING;md5=6b4dd4e68dee6d9e2bb9139a1bf1ada8"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}:"
SRCREV = "${AUTOREV}"
BRANCH = "dev"

SRC_URI = "git://${HOME}/HardBlare/kblare-tools;protocol=file;barcelone=1;branch=${BRANCH}"
S = "${WORKDIR}/git"

#inherit autotools

PARALLEL_MAKE = ""

do_compile () {
    # You will almost certainly need to add additional arguments here
    oe_runmake
}
	
do_install () {
    # This is a guess; additional arguments may be required
    #    oe_runmake install DESTDIR=${D}
    install -d ${D}${bindir}
    install -m 0755 delinfo ${D}${bindir}
    install -m 0755 getinfo ${D}${bindir}
    install -m 0755 list_xattr ${D}${bindir}
    install -m 0755 setinfo ${D}${bindir}
}




